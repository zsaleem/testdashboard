import Dexie from 'dexie';
import Plotly from 'plotly.js';

const db = new Dexie('dashboard');

db.version(1).stores({
    cityStatistics: 'City, Population'
});

let cities = [];
let population = [];

class Index {

    constructor() {
        this.isInternetAvailable();

        setInterval(this.isInternetAvailable.bind(this), 10000);
    }

    isInternetAvailable() {
        if (window.navigator.onLine) {
            this.getChartDataWhenOnline();
        } else {
            this.getChartDataWhenOffline();
        }
    }

    getChartDataWhenOnline() {
        console.log('ONLINE');

        return window.fetch('https://sheetsu.com/apis/v1.0/02d5324c9640', {
            mode: 'cors',
            method: 'GET'
        }).then(response => {
            return response.json();
        }).then(data => {
            let citiesNames = [];
            let citiesPopulation = [];

            data.forEach((item, index) => {
                db.cityStatistics.put(item);
                cities.push(item.City);
                population.push(item.Population);
            });

            this.renderChart();

        }).catch(error => {
            console.log('ERROR: ', error);
        });
    }

    getChartDataWhenOffline() {
        console.log('OFFLINE');

        let citiesNames = [];
        let citiesPopulation = [];

        if (cities.length > 0 && population.length > 0) {
            this.renderChart();
        } else {
            db.cityStatistics.each(item => {
                cities.push(item.City);
                population.push(item.Population);
            }).then(res => {
                this.renderChart();
            });
        }
    }

    renderChart() {
        let $container = document.querySelector('.container');

        cities.sort();
        population.sort();

        let chartData = [
            {
                x: cities,
                y: population,
                type: 'bar'
            }
        ];

        Plotly.newPlot($container, chartData);
    }

}

export default Index;

